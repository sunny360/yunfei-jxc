package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.business.model.Memorandum;
import com.yunfeisoft.business.service.inter.MemorandumService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.service.inter.DataService;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: MemorandumController
 * Description: 备忘录信息Controller
 * Author: Jackie liu
 * Date: 2020-08-23
 */
@Controller
public class MemorandumController extends BaseController {

    @Autowired
    private MemorandumService memorandumService;
    @Autowired
    private DataService dataService;

    /**
     * 添加备忘录信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/memorandum/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(Memorandum record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "title", "标题为空");
        validator.required(request, "beginDate", "开始时间为空");
        validator.required(request, "content", "内容为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        memorandumService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改备忘录信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/memorandum/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(Memorandum record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        validator.required(request, "title", "标题为空");
        validator.required(request, "beginDate", "开始时间为空");
        validator.required(request, "content", "内容为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        memorandumService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询备忘录信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/memorandum/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        Memorandum record = memorandumService.load(id);
        if (record != null) {
            String content = dataService.loadByRefId(id);
            record.setContent(content);
        }
        return ResponseUtils.success(record);
    }

    /**
     * 查询备忘录信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/memorandum/share", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response share(HttpServletRequest request, HttpServletResponse response) {
        return query(request, response);
    }

    /**
     * 分页查询备忘录信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/memorandum/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String title = ServletRequestUtils.getStringParameter(request, "title", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("title", title);
        params.put("orgId", user.getOrgId());

        Page<Memorandum> page = memorandumService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除备忘录信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/memorandum/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        memorandumService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}
