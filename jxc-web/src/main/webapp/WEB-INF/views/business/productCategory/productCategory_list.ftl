<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>商品类别列表</title>
    <link rel="stylesheet" type="text/css" href="${params.contextPath}/common/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<#include "/common/vue_resource.ftl">
    <script type="text/javascript" src="${params.contextPath}/common/ztree/js/jquery.ztree.all.min.js"></script>
</head>
<body>
<div id="app" v-cloak>
    <div class="layui-row layui-col-space10" @click="hideMenu" style="padding:10px;">
        <div class="layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">类别树</div>
                <div class="layui-card-body" ref="tree">
                    <ul id="tree" class="ztree"></ul>
                </div>
            </div>
        </div>
        <div class="layui-col-md9">
            <div class="layui-card">
                <div class="layui-card-body">
                    <div class="layui-row app-header">
                        <div class="layui-col-md6">
                            <div class="layui-btn-group">
                                <@auth code='productCategory_add'>
                                    <button type="button" class="layui-btn layui-btn-sm" @click="add">创建商品类别</button>
                                    <button type="button" class="layui-btn layui-btn-sm" @click="importExcel">导入Excel</button>
                                    <a class="layui-btn layui-btn-sm" href="${params.contextPath}/excel/商品类别_模板.xlsx">下载Excel模板</a>
                                </@auth>
                                <button type="button" class="layui-btn layui-btn-sm" @click="exportExcel">导出Excel</button>
                            </div>
                        </div>
                        <div class="layui-col-md6 text-right">
                            <input type="text" v-model="params.name" placeholder="类别名称" class="layui-input">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
                        </div>
                    </div>
                    <div class="app-list">
                        <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                            <thead>
                            <tr>
                                <th style="width:20px;">#</th>
                                <th>编码</th>
                                <th>名称</th>
                                <th>名称路径</th>
                                <th>拼音简码</th>
                                <th style="width:100px;">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(item, index) in rows">
                                <td>{{20 * (params.page - 1) + 1 + index}}</td>
                                <td>{{item.code}}</td>
                                <td>{{item.name}}</td>
                                <td>{{item.namePath}}</td>
                                <td>{{item.pinyinCode}}</td>
                                <td class="more-parent">
                                    <@auth code='productCategory_update'>
                                        <div class="ui-operating" @click="modify(index)">编辑</div>
                                        <div class="ui-split"></div>
                                    </@auth>
                                    <@auth code='productCategory_delete'><div class="ui-operating" @click="remove(index)">删除</div></@auth>
                                    <#--<div class="ui-operating" @click.stop="showMenu(index)">更多</div>
                                    <div class="more-container" v-if="item.showMenu">
                                        <div class="more-item" @click="remove(index)">删除</div>
                                    </div>-->
                                </td>
                            </tr>
                            <tr v-if="rows.length <= 0">
                                <td colspan="6" class="text-center">没有更多数据了...</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="layui-row">
                            <div class="layui-col-md6">
                                <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                            </div>
                            <div class="layui-col-md6 text-right">
                                <span class="prev" @click="loadPrev">上一页</span>
                                <span class="next" @click="loadNext">下一页</span>
                            </div>
                        </div>
                    </div>
                    <#--end-->
                </div>
            </div>
        </div>
    </div>

    <div style="display:none">
        <form @submit.prevent="uploadFileForm()" method="post">
            <input type="file" name="file" id="uploadFile" ref="fileUpload" single accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
        </form>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                parentId:'',
                name: '',
                page: 1,
            },
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.init();
            this.loadTree();
        },
        methods: {
            init: function () {
                var clientHeight = document.documentElement.clientHeight - 90;
                this.$refs.tree.style.height = clientHeight + 'px';
            },
            seachData: function () {
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            treeClick: function (event, treeId, treeNode, clickFlag) {
                if (treeNode.id == "1") {
                    this.params.parentId = "";
                } else {
                    this.params.parentId = treeNode.id;
                }
                this.seachData();
            },
            beforeDrop: function (treeId, treeNodes, targetNode, moveType, isCopy) {
                var success = false;
                $.loading();
                $.ajax({
                    url: "${params.contextPath}/web/productCategory/drag.json",
                    data: {id: treeNodes[0].id, targetId: targetNode.id},
                    async: false,
                    type: "post",
                    success: function (data) {
                        $.closeLoading();
                        if (!data.success) {
                            $.message("拖拽失败");
                            return;
                        }
                        $.message(data.message);
                        success = true;
                    }
                });
                return success;
            },
            loadTree: function () {
                var that = this;
                var setting = {
                    data: {simpleData: {enable: true}},
                    edit: {
                        drag: {autoExpandTrigger: true},
                        enable: true,
                        showRemoveBtn: false,
                        showRenameBtn: false
                    },
                    callback: {onClick: this.treeClick, beforeDrop: this.beforeDrop}
                };
                $.http.post('${params.contextPath}/web/productCategory/loadTree.json').then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var list = data.data;
                    list.push({id: "1", pid: '0', name: '所有类别'});
                    var zTree = $.fn.zTree.init($("#tree"), setting, list);
                    var nodes = zTree.transformToArray(zTree.getNodes());
                    zTree.expandNode(nodes[0], true);

                    that.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/productCategory/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            add: function () {
                this.showTypes = false;
                var url = "${params.contextPath!}/view/business/productCategory/productCategory_edit.htm";
                DialogManager.open({url: url, width: '650px', height: '100%', title: '添加商品类别'});
            },
            modify: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/productCategory/productCategory_edit.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '编辑商品类别'});
            },
            showMenu: function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            remove:function (index) {//删除
                if (!confirm("确定删除分类吗？")) {
                    return;
                }
                var that = this;
                $.http.post("${params.contextPath}/web/productCategory/delete.json", {ids: this.rows[index].id}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    that.loadData();
                });
            },
            exportExcel: function () {
                var url = "${params.contextPath}/web/productCategory/exportExcel.htm?name=" + (this.params.name || "");
                location.href = url;
            },
            importExcel: function () {
                var that = this;
                $("#uploadFile").val("").unbind().change(function () {
                    var value = $(this).val();
                    if (!value) {
                        $.message("请选择Excel文件");
                        return;
                    }
                    that.uploadFileForm();
                }).click();
            },
            uploadFileForm: function () {
                var that = this;
                var file = this.$refs.fileUpload.files[0];
                var data = new FormData();
                data.append('file', file);
                axios.post('${params.contextPath}/web/productCategory/importExcel.json', data, {
                    headers: {'Content-Type': 'multipart/form-data'},
                }).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    that.loadData();
                });
            }
        }
    });
</script>
</body>

</html>
