<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>库存变化历史</title>
	<#include "/common/vue_resource.ftl">
    <style>
        .layui-table tbody tr:last-child{background-color1:#1E9FFF !important;color:#FF5722;}
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container">
        <div class="app-list">
            <#--<div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">商品列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>-->
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>日期</th>
                    <th>初始库存</th>
                    <th>进货数量</th>
                    <th>销售数量</th>
                    <th>调拨数量</th>
                    <th>损益数量</th>
                    <th>动态数量</th>
                    <th>当前库存</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" style="cursor:pointer;" @click="selectProduct(index)">
                    <td><span v-if="index != rows.length - 1">{{1 + index}}</span></td>
                    <td>{{item.date}}</td>
                    <td><span v-if="index == rows.length - 1 || index == 0">{{item.initStock || 0}}</td>
                    <td>{{item.purchaseNum || 0}}</td>
                    <td>{{item.saleNum || 0}}</td>
                    <td>{{item.allotNum || 0}}</td>
                    <td>{{item.indecNum || 0}}</td>
                    <td>{{item.caclStock || 0}}</td>
                    <td>{{item.stock}}</td>
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="9" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
           <#-- <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>-->
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                productId:'${params.productId!}',
                page: 1,
            },
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/product/stockChange.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.data;
                });
            },
        }
    });
</script>
</body>

</html>
