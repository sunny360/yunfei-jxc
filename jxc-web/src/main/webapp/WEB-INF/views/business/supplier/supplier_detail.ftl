<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>查看供应商信息</title>
    <#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="ui-table-div">
        <table class="layui-table ui-table">
            <tr>
                <th>编码</th>
                <td>{{record.code}}</td>
            </tr>
            <tr>
                <th>名称</th>
                <td>{{record.name}}</td>
            </tr>
            <tr>
                <th>联系人</th>
                <td>{{record.linkman}}</td>
            </tr>
            <tr>
                <th>传真</th>
                <td>{{record.fax}}</td>
            </tr>
            <tr>
                <th>常用电话</th>
                <td>{{record.phone}}</td>
            </tr>
            <tr>
                <th>备用电话</th>
                <td>{{record.standbyPhone}}</td>
            </tr>
            <tr>
                <th>地址</th>
                <td>{{record.address}}</td>
            </tr>
            <tr>
                <th>QQ号</th>
                <td>{{record.qq}}</td>
            </tr>
            <tr>
                <th>微信</th>
                <td>{{record.wechat}}</td>
            </tr>
            <tr>
                <th>旺旺</th>
                <td>{{record.wangwang}}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{{record.email}}</td>
            </tr>
            <tr>
                <th>网址</th>
                <td>{{record.website}}</td>
            </tr>
            <tr>
                <th>银行账户</th>
                <td>{{record.bankAccount}}</td>
            </tr>
            <tr>
                <th>银行户名</th>
                <td>{{record.bankName}}</td>
            </tr>
            <tr>
                <th>账号/卡号</th>
                <td>{{record.bankNo}}</td>
            </tr>
            <tr>
                <th>累积付款</th>
                <td>{{record.balance}}</td>
            </tr>
            <tr>
                <th>拼音简码</th>
                <td>{{record.pinyinCode}}</td>
            </tr>
            <tr>
                <th>备注</th>
                <td>{{record.remark}}</td>
            </tr>
        </table>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            record : {},
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            loadData: function () {
                if (!'${params.id!}') {
                    return;
                }
                var that = this;
                $.http.post('${params.contextPath}/web/supplier/query.json', {id: '${params.id!}'}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.record = data.data;
                });
            },
        }
    });
</script>
</body>

</html>