<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>供应商列表</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <div class="layui-col-md6">
                <div class="layui-btn-group">
                    <@auth code='supplier_add'>
                        <button type="button" class="layui-btn layui-btn-sm" @click="add">创建供应商</button>
                        <button type="button" class="layui-btn layui-btn-sm" @click="importExcel">导入Excel</button>
                        <a class="layui-btn layui-btn-sm" href="${params.contextPath}/excel/供应商_模板.xlsx">下载Excel模板</a>
                    </@auth>
                    <button type="button" class="layui-btn layui-btn-sm" @click="exportExcel">导出Excel</button>
                    <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="top.showNotice('supplier_list')">使用技巧</button>
                </div>
            </div>
            <div class="layui-col-md6 text-right">
                <input type="text" v-model="params.name" placeholder="供应商名称" class="layui-input" @keyup.13="seachData"/>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">供应商列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>编码</th>
                    <th>名称</th>
                    <th>联系人</th>
                    <th>常用电话</th>
                    <th>地址</th>
                    <th>备注</th>
                    <th>累积付款</th>
                    <th>拼音简码</th>
                    <th style="width:125px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" <@auth code='supplier_detail'> @dblclick="showDetail(index)" </@auth>>
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.code}}</td>
                    <td>{{item.name}}</td>
                    <td>{{item.linkman}}</td>
                    <td>{{item.phone}}</td>
                    <td>{{item.address}}</td>
                    <td>{{item.remark}}</td>
                    <td>{{item.balance}}</td>
                    <td>{{item.pinyinCode}}</td>
                    <td class="more-parent">
                        <@auth code='supplier_update'>
                            <div class="ui-operating" @click="modify(index)">编辑</div>
                            <div class="ui-split"></div>
                        </@auth>
                        <@auth code='supplier_detail'>
                            <div class="ui-operating" @click="showDetail(index)">查看</div>
                            <div class="ui-split"></div>
                        </@auth>
                        <@auth code='supplier_delete'>
                            <div class="ui-operating" @click.stop="showMenu(index)">更多</div>
                            <div class="more-container" v-if="item.showMenu">
                                <div class="more-item" @click="remove(index)">删除</div>
                            </div>
                        </@auth>
                    </td>
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="10" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

    <div style="display:none">
        <form @submit.prevent="uploadFileForm()" method="post">
            <input type="file" name="file" id="uploadFile" ref="fileUpload" single accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
        </form>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                name:'',
                page: 1,
            },
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/supplier/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            add:function () {
                this.showTypes = false;
                var url = "${params.contextPath!}/view/business/supplier/supplier_edit.htm";
                DialogManager.open({url:url, width:'650px', height:'100%', title:'添加供应商'});
            },
            modify: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/supplier/supplier_edit.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '编辑供应商'});
            },
            showDetail: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/supplier/supplier_detail.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '查看供应商'});
            },
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            remove:function (index) {//删除
                if (!confirm("确定删除供应商吗？")) {
                    return;
                }
                var that = this;
                $.http.post("${params.contextPath}/web/supplier/delete.json", {ids: this.rows[index].id}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    that.loadData();
                });
            },
            exportExcel: function () {
                var url = "${params.contextPath}/web/supplier/exportExcel.htm?name=" + (this.params.name || "");
                location.href = url;
            },
            importExcel: function () {
                var that = this;
                $("#uploadFile").val("").unbind().change(function () {
                    var value = $(this).val();
                    if (!value) {
                        $.message("请选择Excel文件");
                        return;
                    }
                    that.uploadFileForm();
                }).click();
            },
            uploadFileForm: function () {
                var that = this;
                var file = this.$refs.fileUpload.files[0];
                var data = new FormData();
                data.append('file', file);
                axios.post('${params.contextPath}/web/supplier/importExcel.json', data, {
                    headers: {'Content-Type': 'multipart/form-data'},
                }).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    that.loadData();
                });
            }
        }
    });
</script>
</body>

</html>
