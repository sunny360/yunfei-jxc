package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.CodeBuilder;

import java.util.Map;

/**
 * ClassName: CodeBuilderService
 * Description: 编码生成记录service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface CodeBuilderService extends BaseService<CodeBuilder, String> {

    public Page<CodeBuilder> queryPage(Map<String, Object> params);

    /**
     * 生成编码
     *
     * @param type   编码类型
     * @param length 长度
     * @return
     */
    public String generateCode(String type, int length, String orgId);

    /**
     * 获取销售单编码
     *
     * @param orgId
     * @return
     */
    public String generateSaleOrderCode(String orgId);

    /**
     * 获取采购单编码
     *
     * @param orgId
     * @return
     */
    public String generatePurchaseOrderCode(String orgId);

    /**
     * 获取调拨单编码
     *
     * @param orgId
     * @return
     */
    public String generateAllotOrderCode(String orgId);

    /**
     * 获取损益单编码
     *
     * @param orgId
     * @return
     */
    public String generateIndecOrderCode(String orgId);
}