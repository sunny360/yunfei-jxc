package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.Customer;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: CustomerService
 * Description: 客户信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface CustomerService extends BaseService<Customer, String> {

    public Page<Customer> queryPage(Map<String, Object> params);

    public List<Customer> queryList(Map<String, Object> params);

    public boolean isDupName(String orgId, String id, String name);
}