package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.PaymentRecord;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: PaymentRecordDao
 * Description: 付款信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface PaymentRecordDao extends BaseDao<PaymentRecord, String> {

    public Page<PaymentRecord> queryPage(Map<String, Object> params);

    public List<PaymentRecord> queryByPurchaseOrderId(String purchaseOrderId);

    public int removeByPurchaseOrderId(String purchaseOrderId);
}