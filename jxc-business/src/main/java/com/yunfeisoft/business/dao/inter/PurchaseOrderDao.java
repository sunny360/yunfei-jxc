package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.PurchaseOrder;

import java.util.Map;

/**
 * ClassName: PurchaseOrderDao
 * Description: 采购单信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface PurchaseOrderDao extends BaseDao<PurchaseOrder, String> {

    public Page<PurchaseOrder> queryPage(Map<String, Object> params);

    public PurchaseOrder queryTotalAmount(String orgId, int status, int payStatus);
}