package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.MemorandumDao;
import com.yunfeisoft.business.model.Memorandum;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: MemorandumDaoImpl
 * Description: 备忘录信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-23
 */
@Repository
public class MemorandumDaoImpl extends ServiceDaoImpl<Memorandum, String> implements MemorandumDao {

    @Override
    public Page<Memorandum> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("beginDate");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("title", params.get("title"));
        }
        return queryPage(wb);
    }
}