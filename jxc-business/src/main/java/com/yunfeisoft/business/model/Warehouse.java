package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ClassName: Warehouse
 * Description: 仓库信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-04
 */
@Entity
@Table(name = "TT_WAREHOUSE")
public class Warehouse extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 编码
     */
    @Column
    private String code;

    /**
     * 名称
     */
    @Column
    private String name;

    /**
     * 是否默认：1是 2否
     */
    @Column
    private Integer isDefault;


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }
}